provider "azurerm" {
		subscription_id="b982155f-abd7-4301-9199-6fc2a177e678"
		client_id="36358cbd-8e8a-4ff0-b045-4ae5d6d87091"
		client_secret="13453aa7-c015-43c5-80c9-b90528dd0f37"
		tenant_id="34d903b5-7734-40e1-af8a-1102173fa903"

	}
	resource "azurerm_resource_group" "myterraformgroup" {
		name = "myResourceGroup"
		location = "eastus"
		tags {
		environment = "Terraform Demo"
		}
	}
	resource "azurerm_virtual_network" "myterraformnetwork" {
		name = "myVnet"
		address_space = ["10.0.0.0/16"]
		location = "eastus"
		resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
	
		tags {
		environment = "Terraform Demo"
		}
	}
	resource "azurerm_public_ip" "myterraformpublicip" {
		name = "myPublicIP"
		location = "eastus"
		resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
		public_ip_address_allocation = "dynamic"
                domain_name_label = "team-mad"
	
		tags {
		environment = "Terraform Demo"
		}
	}
	resource "azurerm_subnet" "myterraformsubnet" {
		name = "mySubnet"
		resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
		virtual_network_name = "${azurerm_virtual_network.myterraformnetwork.name}"
		address_prefix = "10.0.2.0/24"
	}
	resource "azurerm_network_security_group" "myterraformnsg" {
		name = "myNetworkSecurityGroup"
		location = "eastus"
		resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
	
		security_rule {
		name = "SSH"
		priority = 1001
		direction = "Inbound"
		access = "Allow"
		protocol = "Tcp"
		source_port_range = "*"
		destination_port_range = "22"
		source_address_prefix = "*"
		destination_address_prefix = "*"
		}

		security_rule {
		name = "HTTP"
		priority = 1003
		direction = "Inbound"
		access = "Allow"
		protocol = "Tcp"
		source_port_range = "*"
		destination_port_ranges = ["8080","8085"]
		source_address_prefix = "*"
		destination_address_prefix = "*"
		}
		tags {
		environment = "Terraform Demo"
		}
	}
	resource "azurerm_network_interface" "myterraformnic" {
	
		name = "myNIC"
		location = "eastus"
		resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
	
		ip_configuration {
		name = "myNicConfiguration"
		subnet_id = "${azurerm_subnet.myterraformsubnet.id}"
		private_ip_address_allocation = "dynamic"
		public_ip_address_id = "${azurerm_public_ip.myterraformpublicip.id}"
		}
	
		tags {
		environment = "Terraform Demo"
		}
	}
	resource "random_id" "randomId" {
		keepers = {
		# Generate a new ID only when a new resource group is defined
		resource_group = "${azurerm_resource_group.myterraformgroup.name}"
		}
		
		byte_length = 8
	}
	resource "azurerm_storage_account" "mystorageaccount" {
		name = "diag${random_id.randomId.hex}"
		resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
		location = "eastus"
		account_replication_type = "LRS"
		account_tier = "Standard"
	
		tags {
		environment = "Terraform Demo"
		}
	}
	resource "azurerm_virtual_machine" "myterraformvm" {
		name = "devops-app-01"
		location = "eastus"
		resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
		network_interface_ids = ["${azurerm_network_interface.myterraformnic.id}"]
		vm_size = "Standard_DS1_v2"
	
		storage_os_disk {
		name = "myOsDisk"
		caching = "ReadWrite"
		create_option = "FromImage"
		managed_disk_type = "Premium_LRS"
		}
	
		storage_image_reference {
		publisher = "Canonical"
		offer = "UbuntuServer"
		sku = "16.04.0-LTS"
		version ="latest"
		}
		
		os_profile {
		computer_name = "devops-app-01"
		admin_username = "stage"
		
		}
		
		os_profile_linux_config {
		disable_password_authentication = true
		ssh_keys {
		path ="/home/stage/.ssh/authorized_keys"
		key_data ="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDLM9LoZz1CSppnCH5d5k9+eqb25tznTpDXkV47kaxJH17wxKSteL7g1incPo9hbuoffvYLpw1zM9TabRMDamDAtr8wXtQtV9/LCpZ/U8hiypyw01Egxrrpzh2cxpv92FOlBMF197j0LyxONuNdBhR0a4u6IdGRcdvS+gOqi1nQ/vwrgeB4vFcnnZ7VaUY7r7kbMt4g3bM2Ng5gM5ERsZjFhyiXaV/2pnRWubEi6Vkd+2UghX85rpFxfzYR1/G3ro+/0LY6jaf50re0FsLxt8C0eVmr2BOGYFwispyU/UiQ2+ved5MTAkvO7zuQlL+U3N5rAXf/w9+9pizqVaDQL0CF stage@localhost.localdomain"
		}
		}
	
		boot_diagnostics {
		enabled = "true"
		storage_uri = "${azurerm_storage_account.mystorageaccount.primary_blob_endpoint}"
		}
	
		tags {
		environment = "Terraform Demo"
		}
	}


